const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const salty = 10;

const UserSchema = new mongoose.Schema({
    username: {type: String, required: true, unique: true},
    email: {type: String, required: true, unique: true},
    pass: {type: String, required: true, unique: true}
})

UserSchema.pre('save', function(next) {
    if(this.isNew || this.isModified('password')) {
        const doc = this;
        bcrypt.hash(doc.pass, salty, function(err, hashedPassword) {
            if(err){
                next(err);
            }else{
                doc.pass = hashedPassword;
                next();
            }
        })
    }else{
        next();
    }
})

UserSchema.methods.isCorrectPassword = (password, callback) => {
    bcrypt.compare(password, this.password, (err, same) => {
        if(err){
            callback(err);
        }else{
            callback(err, same);
        }
    })
}

module.exports = mongoose.model('User', UserSchema)