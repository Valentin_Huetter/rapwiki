const express = require('express');
const http = require('http');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const User = require('./api/models/User');
const jwt = require('jsonwebtoken');
const cookie = require('cookie-parser');
const auth = require('./auth');

const app = express();
http.createServer(app);
const port = 8080;

const mongoUrl = "mongodb://admin:admin123@ds113454.mlab.com:13454/rapusers";
const secret = "asbdjasbd";

mongoose.connect(mongoUrl, function(err) {
    if (err) {
      throw err;
    } else {
      console.log(`Successfully connected to ${mongoUrl}`);
    }
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookie());

app.get('/', (req, res, next) => {
    res.send('Welcome to the api');
})

app.get('/needAuth', auth.withAuth, function(req, res)  {
    res.send('You entered Secret Auth Area');
})

app.get('/authCheck', auth.withAuth, (req, res) => {
    res.sendStatus(200);
})

app.post('/register', (req, res) => {
    const { username, email, pass} = req.body;
    const user = new User({username, email, pass});
    if(User.findOne(user)){
        res.status(200).send('User already registered');
    }
    user.save((err) => {
        if(err){
            res.status(500).send('Error registering user');
        }else{
            res.status(200).send('User successfully registered');
        }
    })
})

app.post('/authenticate', function(req, res) {
    const { username, password } = req.body;
    User.findOne({ username }, function(err, user) {
      if (err) {
        console.error(err);
        res.status(500)
          .json({
          error: 'Internal error please try again'
        });
      } else if (!user) {
        res.status(401)
          .json({
            error: 'Incorrect Username or Password'
          });
      } else {
        user.isCorrectPassword(password, function(err, same) {
          if (err) {
            res.status(500)
              .json({
                error: 'Internal error please try again'
            });
          } else if (!same) {
            res.status(401)
              .json({
                error:'Incorrect Username or Password'
            });
          } else {
            const payload = { username };
            const token = jwt.sign(payload, secret, {
              expiresIn: '1h'
            });
            res.cookie('token', token, { httpOnly: true })
              .sendStatus(200);
          }
        });
      }
    });
  });

app.listen(port, () =>{
    console.log(`Server running on Port: ${port}`)
})